# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import (fields, ModelView, ModelSQL, ModelSingleton,
    MultiValueMixin)
from .machinery import KINDS


class Configuration(ModelSingleton, ModelSQL, ModelView, MultiValueMixin):
    """Configuration"""
    __name__ = 'machinery.configuration'

    kinds = fields.MultiSelection(KINDS, 'Kinds')

    def get_kinds(self):
        selection = self.fields_get(['kinds'])['kinds']['selection']
        if self.kinds:
            selection = [
                (k, v) for k, v in selection if k in self.kinds]
        return selection
