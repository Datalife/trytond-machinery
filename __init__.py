# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import machinery
from . import party
from . import configuration


def register():
    Pool.register(
        configuration.Configuration,
        machinery.Machinery,
        machinery.PartyMachinery,
        party.Party,
        module='machinery', type_='model')
