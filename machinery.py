# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import (fields, ModelView, ModelSQL, DeactivableMixin,
        sequence_ordered, MatchMixin)
from trytond.pyson import Eval, Id, Not, In
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.modules.product import price_digits
from sql.aggregate import Min
from trytond import backend
from sql import Table, Literal

KINDS = [
    ('tractor', 'Tractor'),
    ('barrel', 'Barrel'),
    ('trailer', 'Trailer'),
    ('other', 'Other'),
]


class Machinery(DeactivableMixin, ModelView, ModelSQL):
    """Machinery"""
    __name__ = 'machinery.machinery'

    kind = fields.Selection('get_kinds', 'Kind', required=True)
    code = fields.Char('Code')
    name = fields.Char('Name', translate=True,
        required=True)
    brand = fields.Char('Brand')
    model = fields.Char('Model')
    registration_number = fields.Char('Registration number')
    acquisition_date = fields.Date('Acquisition date')
    inspection_date = fields.Date('Inspection date')
    capacity = fields.Float('Capacity', digits=(16, 4))
    capacity_uom = fields.Many2One('product.uom', 'Capacity UOM',
        domain=[('category', 'in', [
            Id('product', 'uom_cat_volume'),
            Id('product', 'uom_cat_weight')])])
    capacity_digits = fields.Function(fields.Integer('Capacity Digits'),
        'on_change_with_capacity_digits')
    cost_price = fields.Numeric('Cost Price', digits=price_digits,
        required=True, help='Hourly cost price')
    distance = fields.Function(fields.Integer('Distance'), 'get_distance')
    automatic = fields.Boolean('Automatic',
        states={
            'invisible': (Eval('kind') != 'barrel')
        },
        depends=['kind'])

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)
        cursor = Transaction().connection.cursor()
        sql_table = cls.__table__()
        type_ = Table('machinery_type')

        # Migration from 4.8: remove agro nomenclature
        old_table = 'agro_machinery'
        if table_h.table_exist(old_table):
            table_h.table_rename(old_table, cls._table)

        if table_h.column_exist('type'):
            table_h.column_rename('type', 'kind')
        machinery_type_exist = table_h.column_exist('machinery_type')

        super().__register__(module_name)

        if (machinery_type_exist
                and backend.TableHandler.table_exist('machinery_type')):
            cursor.execute(*sql_table.update(
                columns=[sql_table.kind],
                values=[type_.select(type_.kind,
                    where=type_.id == sql_table.machinery_type)]))

        if machinery_type_exist:
            table_h.drop_column('machinery_type')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._order.insert(0, ('distance', 'ASC NULLS LAST'))
        cls._order.insert(1, ('name', 'ASC'))
        required_domain = In(Eval('kind'), cls.get_capacity_kinds())
        invisible_domain = Not(In(Eval('kind'), cls.get_capacity_kinds()))
        depends = ['kind']
        for fieldname in ('capacity', 'capacity_uom'):
            _field = getattr(cls, fieldname, None)
            _field.states['required'] = required_domain
            _field.states['invisible'] = invisible_domain
            _field.depends = depends

    @staticmethod
    def get_kinds():
        pool = Pool()
        Configuration = pool.get('machinery.configuration')
        config = Configuration(1)
        return config.get_kinds()

    @classmethod
    def get_capacity_kinds(cls):
        return ['barrel', 'trailer']

    @classmethod
    def default_capacity_uom(cls):
        pool = Pool()
        Modeldata = pool.get('ir.model.data')
        return Modeldata.get_id('product', 'uom_liter')

    @fields.depends('capacity_uom')
    def on_change_with_capacity_digits(self, name=None):
        if self.capacity_uom:
            return self.capacity_uom.digits
        return 2

    @classmethod
    def _distance_query(cls, usages=None, party=None, depth=None):
        context = Transaction().context
        if party is None:
            party = context.get('related_party')

        if not party:
            return

        MachineryParty = Pool().get('machinery.machinery-party.party')
        table = MachineryParty.__table__()
        return table.select(
            table.machinery.as_('to'),
            Literal(0).as_('distance'),
            where=(table.party == party))

    @classmethod
    def get_distance(cls, records, name):
        distances = {r.id: None for r in records}
        query = cls._distance_query()
        if query:
            cursor = Transaction().connection.cursor()
            cursor.execute(*query.select(
                    query.to.as_('to'),
                    Min(query.distance).as_('distance'),
                    group_by=[query.to]))
            distances.update(cursor)
        return distances

    @classmethod
    def order_distance(cls, tables):
        machinery, _ = tables[None]
        key = 'distance'
        if key not in tables:
            query = cls._distance_query()
            if not query:
                return []
            query = query.select(
                    query.to.as_('to'),
                    Min(query.distance).as_('distance'),
                    group_by=[query.to])
            join = machinery.join(query, type_='LEFT',
                condition=query.to == machinery.id)
            tables[key] = {
                None: (join.right, join.condition),
                }
        else:
            query, _ = tables[key][None]
        return [query.distance]


class PartyMachinery(sequence_ordered(), MatchMixin, ModelSQL, ModelView):
    """Party - Machinery"""
    __name__ = 'machinery.machinery-party.party'

    party = fields.Many2One('party.party', 'Party', required=True,
        ondelete='CASCADE')
    machinery = fields.Many2One('machinery.machinery', 'Machinery',
        required=True, ondelete='CASCADE')

    def match(self, pattern, match_none=False):
        pattern = pattern.copy()

        def matching_kind(key):
            kind = pattern.pop(key)
            if not isinstance(kind, (list, tuple, set)):
                kind = [kind]
            return set(kind) & set([self.machinery.kind])

        if 'kind' in pattern and not matching_kind('kind'):
            return False
        if 'not_kind' in pattern and matching_kind('not_kind'):
            return False
        return super().match(pattern, match_none=match_none)
