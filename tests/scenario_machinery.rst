==================
Machinery Scenario
==================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> import datetime
    >>> from decimal import Decimal
    >>> from dateutil.relativedelta import relativedelta
    >>> today = datetime.date.today()
    >>> yesterday = today - relativedelta(days=1)

Install machinery::

    >>> config = activate_modules('machinery')

Get models::

    >>> Machinery = Model.get('machinery.machinery')
    >>> Configuration = Model.get('machinery.configuration')
    >>> ProductUom = Model.get('product.uom')


Add kind other in machinery configuration::

    >>> machinery_config = Configuration(1)
    >>> machinery_config.kinds = ['other']
    >>> machinery_config.save()


Create tractor machinery::

    >>> tractor = Machinery()
    >>> tractor.kind = 'tractor'
    >>> tractor.code = '1'
    >>> tractor.name = 'My tractor'
    >>> tractor.brand = 'Yellow tractors'
    >>> tractor.model = 'Yellow tractor'
    >>> tractor.registration_number = '2009-X'
    >>> tractor.acquisition_date = today
    >>> tractor.inspection_date = yesterday
    >>> tractor.cost_price = Decimal(5000)
    >>> tractor.save()
    Traceback (most recent call last):
      ...
    trytond.model.modelstorage.SelectionValidationError: The value "tractor" for field "Kind" in "Machinery" is not one of the allowed options. - 

    >>> machinery_config.kinds = ['tractor']
    >>> machinery_config.save()

    >>> tractor.save()


Create barrel machinery::

    >>> barrel = Machinery()
    >>> barrel.kind = 'barrel'
    >>> barrel.code = '3'
    >>> barrel.name = 'Barrel'
    >>> barrel.brand = 'Barrel free'
    >>> barrel.registration_number = '1950-X'
    >>> barrel.acquisition_date = today
    >>> barrel.inspection_date = yesterday
    >>> barrel.cost_price = Decimal(0)
    >>> barrel.save()
    Traceback (most recent call last):
    ...
    trytond.model.modelstorage.RequiredValidationError: A value is required for field "Capacity" in "Machinery". - 
    >>> barrel.capacity = Decimal(10)
    >>> barrel.save()
    Traceback (most recent call last):
      ...
    trytond.model.modelstorage.SelectionValidationError: The value "barrel" for field "Kind" in "Machinery" is not one of the allowed options. - 

    >>> machinery_config.kinds = ['tractor', 'barrel']
    >>> machinery_config.save()

    >>> barrel.save()


Get ProductUoms::

    >>> kg, = ProductUom.find([('name', '=', 'Kilogram')], limit=1)
    >>> L, = ProductUom.find([('name', '=', 'Liter')], limit=1)


Create trailer::

    >>> trailer = Machinery()
    >>> trailer.kind = 'trailer'
    >>> trailer.code = '2'
    >>> trailer.name = 'Trailer'
    >>> trailer.brand = 'Trailer machinery'
    >>> trailer.model = 'T AE'
    >>> trailer.registration_number = 'T-XII'
    >>> trailer.acquisition_date = today
    >>> trailer.inspection_date = yesterday
    >>> trailer.cost_price = Decimal(1000)
    >>> trailer.save()
    Traceback (most recent call last):
      ...
    trytond.model.modelstorage.RequiredValidationError: A value is required for field "Capacity" in "Machinery". - 
    >>> trailer.capacity = Decimal(800)
    >>> trailer.capacity_uom = kg
    >>> trailer.save()
    Traceback (most recent call last):
      ...
    trytond.model.modelstorage.SelectionValidationError: The value "trailer" for field "Kind" in "Machinery" is not one of the allowed options. - 

    >>> machinery_config.kinds = ['tractor', 'barrel', 'trailer']
    >>> machinery_config.save()

    >>> trailer2 = Machinery()
    >>> trailer2.kind = 'trailer'
    >>> trailer2.code = '2'
    >>> trailer2.name = 'Trailer'
    >>> trailer2.brand = 'Trailer machinery'
    >>> trailer2.model = 'T AE'
    >>> trailer2.registration_number = 'T-XII'
    >>> trailer2.acquisition_date = today
    >>> trailer2.inspection_date = yesterday
    >>> trailer2.cost_price = Decimal(1000)
    >>> trailer2.capacity = Decimal(800)
    >>> trailer2.capacity_uom = L
    >>> trailer2.save()


Create other machinery::

    >>> other = Machinery()
    >>> other.kind = 'other'
    >>> other.code = '2'
    >>> other.name = 'Other'
    >>> other.brand = 'Other machinery'
    >>> other.model = 'X AE'
    >>> other.registration_number = 'A-XII'
    >>> other.acquisition_date = today
    >>> other.inspection_date = yesterday
    >>> other.cost_price = Decimal(1000)
    >>> other.save()
    Traceback (most recent call last):
      ...
    trytond.model.modelstorage.SelectionValidationError: The value "other" for field "Kind" in "Machinery" is not one of the allowed options. - 

    >>> machinery_config.kinds = ['tractor', 'barrel', 'trailer', 'other']
    >>> machinery_config.save()

    >>> other.save()
