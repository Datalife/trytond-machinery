datalife_machinery
==================

The machinery module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-machinery/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-machinery)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
